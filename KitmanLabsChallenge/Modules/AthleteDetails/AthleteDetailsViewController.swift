//
//  AthleteDetailsViewController.swift
//  KitmanLabsChallenge
//
//  Created by Vesela Ilchevska on 11.07.21.
//

import UIKit

protocol AthleteDetailsViewControllerDelegate {
    var title: String { get }
    func getData() -> AthleteHelper
    func loadData(completion: @escaping () -> Void)
}

final class AthleteDetailsViewController: UIViewController {
    private enum Constant {
        static let leadingTrailingConstant: CGFloat = 20
    }
    private lazy var detailsView: DetailsView = {
        let detailsView = DetailsView()
        detailsView.translatesAutoresizingMaskIntoConstraints = false
        return detailsView
    }()

    private var activityView: UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(style: .medium)
        return activityView
    }()

    fileprivate var viewModel: AthleteDetailsViewControllerDelegate!


    override func viewDidLoad() {
        super.viewDidLoad()
        configureActivityView()
        activityView.startAnimating()
        viewModel.loadData { [weak self] in
            DispatchQueue.main.async {
                self?.activityView.stopAnimating()
                self?.configureDetailsView()
            }
        }
        navigationItem.title = viewModel.title
    }
    
    private func configureActivityView() {
        activityView.center = view.center
        activityView.color = .gray
        view.addSubview(activityView)
    }

    private func configureDetailsView() {
        view.addSubview(detailsView)
        NSLayoutConstraint.activate([
            detailsView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            detailsView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            detailsView.leadingAnchor.constraint(equalTo: view.leadingAnchor,
                                                 constant: Constant.leadingTrailingConstant),
            detailsView.trailingAnchor.constraint(equalTo: view.trailingAnchor,
                                                  constant: -Constant.leadingTrailingConstant)
        ])

        detailsView.configureView(with: viewModel.getData())
    }
}

// MARK: - StoryboardInstantiatable

extension AthleteDetailsViewController: StoryboardInstantiatable {
    static var storyboardName: String {
        return Constants.StoryboardNames.athleteDetails
    }
}

struct AthleteDetailsSceneConfigurator {
    func createScene(delegate: AthleteDetailsSceneDelegate, athlete: Athlete) -> AthleteDetailsViewController? {
        let controller = AthleteDetailsViewController.instantiateFromStoryboard()
        let viewModel = AthleteDetailsViewModel(delegate: delegate, athlete: athlete)
        controller?.viewModel = viewModel
        
        return controller
    }
}
