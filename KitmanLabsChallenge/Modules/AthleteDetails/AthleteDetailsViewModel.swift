//
//  AthleteDetailsViewModel.swift
//  KitmanLabsChallenge
//
//  Created by Vesela Ilchevska on 11.07.21.
//

import Foundation

protocol AthleteDetailsSceneDelegate: class {
    
}

final class AthleteDetailsViewModel {
    private weak var delegate: AthleteDetailsSceneDelegate?
    private var athletesProducer: AthletesProducer
    private let athlete: Athlete
    private var squads: [Squad] = []

    init(delegate: AthleteDetailsSceneDelegate,
         athlete: Athlete,
         athletesProducer: AthletesProducer = AthletesProducer()) {
        self.delegate = delegate
        self.athlete = athlete
        self.athletesProducer = athletesProducer
    }
}

// MARK: -AthleteDetailsViewControllerDelegate

extension AthleteDetailsViewModel: AthleteDetailsViewControllerDelegate {
    var title: String {
        return "\(athlete.firstName) \(athlete.lastName)"
    }
    
    func getData() -> AthleteHelper {
        var userSquads: [Squad] = []
        athlete.squadIDS.forEach { squadID in
            if let squad = squads.first(where: { $0.id == squadID }) {
                userSquads.append(squad)
            }
        }
        return AthleteHelper(athlete: athlete,
                             squads: userSquads)
    }
    
    func loadData(completion: @escaping () -> Void) {
        athletesProducer.fetchSquads(completion: { result in
            switch result {
            case .success(let squads):
                self.squads = squads
                completion()
            case .failure, .none:
                break
            }
        })
    }
}
