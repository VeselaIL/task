//  
//  SquadsView.swift
//  KitmanLabsChallenge
//
//  Created by Vesela Ilchevska on 12.07.21.
//

import UIKit

class SquadsView: BaseView {
  
    @IBOutlet private weak var squadNameLabel: UILabel!
    @IBOutlet private weak var createdAtLabel: UILabel!
    @IBOutlet private weak var updatedAt: UILabel!
    @IBOutlet private weak var createdatTextLabel: UILabel!
    @IBOutlet private weak var updatedAtTextLabel: UILabel!
    
    func configure(data: Squad) {
        squadNameLabel.text = data.name
        updatedAt.text = formatDate(from: data.updatedAt)
        createdAtLabel.text = formatDate(from: data.createdAt)
    }
    
    private func formatDate(from date: Date?) -> String {
        guard let date = date else {
            return ""
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        return dateFormatter.string(from: date)
    }
}
