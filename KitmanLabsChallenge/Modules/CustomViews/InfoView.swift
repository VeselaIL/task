//  
//  InfoView.swift
//  KitmanLabsChallenge
//
//  Created by Vesela Ilchevska on 12.07.21.
//

import UIKit

class InfoView: BaseView {
  
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(title: String, description: String) {
        titleLabel.text = title
        descriptionLabel.text = description
    }
}
