//
//  DetailsView.swift
//  KitmanLabsChallenge
//
//  Created by Vesela Ilchevska on 12.07.21.
//

import UIKit
import Kingfisher

class DetailsView: UIView {
    private enum Constants {
        static let regularSpacing: CGFloat = 16
        static let standardSpacing: CGFloat = 8
        static let imageSize: CGFloat = 140
        static let cornerRadius: CGFloat = 10
        static let viewHeight: CGFloat = 3
    }

    private var athleteImageView: UIImageView = {
        let image = UIImageView()
        image.layer.masksToBounds = true
        image.contentMode = .scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    private var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = .lightBlue
        view.layer.cornerRadius = Constants.cornerRadius
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private var firsNameView: InfoView = {
        let infoView = InfoView()
        infoView.translatesAutoresizingMaskIntoConstraints = false
        return infoView
    }()

    private var lastNameView: InfoView = {
        let infoView = InfoView()
        infoView.translatesAutoresizingMaskIntoConstraints = false
        return infoView
    }()
    
    private var userNameView: InfoView = {
        let infoView = InfoView()
        infoView.translatesAutoresizingMaskIntoConstraints = false
        return infoView
    }()
    
    private var verticalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .fill
        stackView.distribution = .fill
        return stackView
    }()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupHeaderView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupHeaderView()
    }
    
    private func setupHeaderView() {
        addSubview(athleteImageView)
        addSubview(separatorView)
        NSLayoutConstraint.activate([
            athleteImageView.topAnchor.constraint(equalTo: topAnchor,
                                                  constant: Constants.regularSpacing),
            athleteImageView.heightAnchor.constraint(equalToConstant: Constants.imageSize),
            athleteImageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            athleteImageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            athleteImageView.bottomAnchor.constraint(equalTo: separatorView.topAnchor,
                                                     constant: -Constants.standardSpacing),
            separatorView.heightAnchor.constraint(equalToConstant: Constants.viewHeight),
            separatorView.leadingAnchor.constraint(equalTo: leadingAnchor),
            separatorView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
        
        setupDetailsView()
    }

    private func setupDetailsView() {
        addSubview(firsNameView)
        addSubview(lastNameView)
        addSubview(userNameView)
        addSubview(verticalStackView)
    
        NSLayoutConstraint.activate([
            firsNameView.topAnchor.constraint(equalTo: separatorView.bottomAnchor),
            firsNameView.bottomAnchor.constraint(equalTo: lastNameView.topAnchor),
            firsNameView.leadingAnchor.constraint(equalTo: leadingAnchor),
            firsNameView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            lastNameView.bottomAnchor.constraint(equalTo: userNameView.topAnchor),
            lastNameView.leadingAnchor.constraint(equalTo: leadingAnchor),
            lastNameView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            userNameView.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor),
            userNameView.leadingAnchor.constraint(equalTo: leadingAnchor),
            userNameView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            verticalStackView.topAnchor.constraint(equalTo: userNameView.bottomAnchor,
                                                   constant: Constants.standardSpacing),
            verticalStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            verticalStackView.leadingAnchor.constraint(equalTo: leadingAnchor)
        ])
    }

    func configureView(with data: AthleteHelper) {
        verticalStackView.arrangedSubviews.forEach({ $0.removeFromSuperview() })
        if let url = URL(string: data.athlete.image.url) {
            athleteImageView.kf.setImage(with: url)
        } else {
            athleteImageView.image = UIImage(named: "default")
        }
        firsNameView.configure(title: "First Name", description: data.athlete.firstName)
        lastNameView.configure(title: "Last Name", description: data.athlete.lastName)
        userNameView.configure(title: "Username", description: data.athlete.username)
        data.squads.forEach { squad in
            let squadView = SquadsView()
            squadView.configure(data: squad)
            verticalStackView.addArrangedSubview(squadView)
            print(squad)
        }
    }
}
