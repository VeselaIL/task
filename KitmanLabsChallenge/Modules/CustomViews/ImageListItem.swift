//  
//  ImageListItem.swift
//  KitmanLabsChallenge
//
//  Created by Vesela Ilchevska on 11.07.21.
//

import UIKit
import Kingfisher

class ImageListItem: UITableViewCell {
    private enum Constants {
        static let regularSpacing: CGFloat = 16
        static let standardSpacing: CGFloat = 8
        static let font: UIFont = AppFont.thin(size: 14)
        static let imageSize: CGFloat = 90
        static let cornerRadius: CGFloat = 10
        static let stackViewMargins = NSDirectionalEdgeInsets(top: 8,
                                                              leading: 16,
                                                              bottom: 8,
                                                              trailing: 16)
        static let height: CGFloat = 100
        static let accessoryHeight: CGFloat = 24
    }

    private var verticalStackView: UIStackView = {
       let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .leading
        stackView.spacing = Constants.regularSpacing
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.directionalLayoutMargins = Constants.stackViewMargins
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()

    private var firstNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Constants.font
        return label
    }()

    private var lastNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Constants.font
        return label
    }()
    
    private var squadsName: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Constants.font
        return label
    }()
    
    private var athleteImageView: UIImageView = {
        let image = UIImageView()
        image.layer.masksToBounds = true
        image.layer.cornerRadius = Constants.cornerRadius
        image.contentMode = .scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()

    private var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "rightArrow")
        return imageView
    }()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    private func commonInit() {
        addSubview(athleteImageView)
        addSubview(verticalStackView)
        verticalStackView.addArrangedSubview(firstNameLabel)
        verticalStackView.addArrangedSubview(lastNameLabel)
        verticalStackView.addArrangedSubview(squadsName)
        addSubview(iconImageView)

        NSLayoutConstraint.activate([
            heightAnchor.constraint(equalToConstant: Constants.height),
            
            athleteImageView.heightAnchor.constraint(equalToConstant: Constants.imageSize),
            athleteImageView.widthAnchor.constraint(equalToConstant: Constants.imageSize),
            athleteImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            athleteImageView.leadingAnchor.constraint(equalTo: leadingAnchor,
                                                      constant: Constants.regularSpacing),
            
            verticalStackView.leadingAnchor.constraint(equalTo: athleteImageView.trailingAnchor,
                                                       constant: Constants.standardSpacing),
            verticalStackView.topAnchor.constraint(equalTo: topAnchor),
            verticalStackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            iconImageView.heightAnchor.constraint(equalToConstant: Constants.accessoryHeight),
            iconImageView.widthAnchor.constraint(equalToConstant: Constants.accessoryHeight),
            iconImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            iconImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.regularSpacing),
            iconImageView.leadingAnchor.constraint(equalTo: verticalStackView.trailingAnchor)
        ])
    }
    
    func configure(with data: Athlete) {
        firstNameLabel.text = "First Name: \(data.firstName)"
        lastNameLabel.text = "Last Name: \(data.lastName)"
        squadsName.text = "UserName: \(data.username)"
        if let url = URL(string: data.image.url) {
            athleteImageView.kf.setImage(with: url)
        } else {
            athleteImageView.image = UIImage(named: "default")
        }
    }
}
