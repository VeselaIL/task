//
//  TextFieldContainerView.swift
//  KitmanLabsChallenge
//
//  Created by Vesela Ilchevska on 10.07.21.
//

import UIKit

final class TextFieldView: BaseView {
    @IBOutlet weak var delegate: UITextFieldDelegate? {
        didSet {
            textField.delegate = delegate
        }
    }
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet private weak var underlineView: UIView!
    
    @IBInspectable var title: String? {
        didSet {
            titleLabel.text = title
        }
    }
    
    @IBInspectable var placeholder: String? {
        didSet {
            textField.attributedPlaceholder = NSAttributedString(string: placeholder ?? "",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: placeholderColor])
        }
    }
    
    @IBInspectable var placeholderColor: UIColor = #colorLiteral(red: 0.8434963226, green: 0.8638278246, blue: 0.883071959, alpha: 1) {
        didSet {
            textField.attributedPlaceholder = NSAttributedString(string: placeholder ?? "",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: placeholderColor])
        }
    }
    
    @IBInspectable var underlineColor: UIColor = #colorLiteral(red: 0.8434963226, green: 0.8638278246, blue: 0.883071959, alpha: 1) {
        didSet {
            underlineView.backgroundColor = underlineColor
        }
    }
    
    @IBInspectable var isSecureTextEntry: Bool = false {
        didSet {
            textField.isSecureTextEntry = isSecureTextEntry
        }
    }
}
