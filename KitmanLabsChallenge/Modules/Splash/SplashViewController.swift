//
//  SpashViewController.swift
//  KitmanLabsChallenge
//
//  Created by Vesela Ilchevska on 10.07.21.
//

import UIKit

protocol SplashSceneDelegate: class {
    func splashSceneDidFinish()
}

final class SplashViewController: UIViewController {

    fileprivate weak var delegate: SplashSceneDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1,
                                      execute: { [weak self] in
                                        self?.delegate?.splashSceneDidFinish()
                                      })
    }
}

extension SplashViewController: StoryboardInstantiatable {
    static var storyboardName: String {
        return Constants.StoryboardNames.splash
    }
}

struct SplashSceneConfigurator {
    func createScene(delegate: SplashSceneDelegate) -> SplashViewController? {
        let splashViewController = SplashViewController.instantiateFromStoryboard()
        splashViewController?.delegate = delegate
        
        return splashViewController
    }
}
