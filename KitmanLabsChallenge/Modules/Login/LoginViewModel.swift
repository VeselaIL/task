//  
//  LoginViewModel.swift
//  KitmanLabsChallenge
//
//  Created by Vesela Ilchevska on 11.07.21.
//

import Foundation

protocol LoginSceneDelegate: class {
    func loginSceneDidLogin(with userName: String)
}

final class LoginViewModel {

    private weak var delegate: LoginSceneDelegate?
    private let athletesProducer: AthletesProducer
    private var usernameValue = ""
    private var passwordValue = ""

    // MARK: - ViewModel
    
    init(delegate: LoginSceneDelegate,
         athletesProducer: AthletesProducer = AthletesProducer()) {
        self.delegate = delegate
        self.athletesProducer = athletesProducer
    }
}

extension LoginViewModel: LoginViewControllerDelegate {
    func usernameEntered(username: String) {
        usernameValue = username
    }
    
    func passwordEntered(password: String) {
        passwordValue = password
    }
    
    func loginTapped() {
        guard !usernameValue.isEmpty, !passwordValue.isEmpty else {
            return
        }
        let userData = UserData(username: usernameValue, password: passwordValue)
        athletesProducer.postUserData(userData: userData) { [weak self] result in
            switch result {
            case .success(let userName):
                DispatchQueue.main.async {
                    self?.delegate?.loginSceneDidLogin(with: userName)
                }
            case .failure, .none:
                break
            }
        }
    }
}
