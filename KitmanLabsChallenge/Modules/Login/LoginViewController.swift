//
//  LoginViewController.swift
//  KitmanLabsChallenge
//
//  Created by Vesela Ilchevska on 10.07.21.
//

import UIKit

protocol LoginViewControllerDelegate {
    /// Handles user's username input
    /// - Parameter username: the user's input
    func usernameEntered(username: String)

    /// Handles user's password input
    /// - Parameter password: the user's input
    func passwordEntered(password: String)

    /// Handles the user's tapping 'Login'
    func loginTapped()
}

final class LoginViewController: UIViewController {

    @IBOutlet private weak var usernameTextFieldView: TextFieldView!
    @IBOutlet private weak var passwordTextFieldView: TextFieldView!
    @IBOutlet private weak var submitButton: UIButton!

    private var activityView: UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(style: .medium)
        return activityView
    }()

    fileprivate var viewModel: LoginViewControllerDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureActivityView()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        activityView.stopAnimating()
    }

    private func configureActivityView() {
        activityView.center = view.center
        activityView.color = .gray
        view.addSubview(activityView)
    }

    @IBAction func submitButtonTouched(_ sender: Any) {
        activityView.startAnimating()
        submitButton.isEnabled = false
        viewModel.loginTapped()
    }
}

// MARK: -UITextFieldDelegate

extension LoginViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        usernameTextFieldView.underlineColor = .lightBlue
        passwordTextFieldView.underlineColor = .lightBlue
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case usernameTextFieldView.textField:
            passwordTextFieldView.textField.becomeFirstResponder()
        case passwordTextFieldView.textField:
            view.endEditing(true)
        default:
            view.endEditing(true)
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let input = textField.text else { return }
        switch textField {
        case usernameTextFieldView.textField:
            viewModel.usernameEntered(username: input)
        case passwordTextFieldView.textField:
            viewModel.passwordEntered(password: input)
        default:
            break
        }
    }
}

extension LoginViewController: StoryboardInstantiatable {
    static var storyboardName: String {
        // Provide the desired storyboard name for ExampleTableViewViewController
        return Constants.StoryboardNames.login
    }
}
struct LoginSceneConfigurator {
    func createScene(delegate: LoginSceneDelegate) -> LoginViewController? {
        let controller = LoginViewController.instantiateFromStoryboard()
        let viewModel = LoginViewModel(delegate: delegate)
        controller?.viewModel = viewModel
        
        return controller
    }
}
