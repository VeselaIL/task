//
//  LoginCoordinator.swift
//  KitmanLabsChallenge
//
//  Created by Vesela Ilchevska on 11.07.21.
//

import UIKit

protocol LoginCoordinatorDelegate: class {
    func loginModuleDidLogin(with userName: String)
}

class LoginCoordinator: Coordinator {
    private weak var delegate: LoginCoordinatorDelegate?
    private let window: UIWindow?
    
    init(window: UIWindow, delegate: LoginCoordinatorDelegate) {
        self.window = window
        self.delegate = delegate
        super.init()
    }
    
    override func start() {
        showLoginScene()
    }
    
    private func showLoginScene() {
        guard let loginViewController = LoginSceneConfigurator().createScene(delegate: self) else { return }
        window?.rootViewController = loginViewController
    }
}

extension LoginCoordinator: LoginSceneDelegate {
    func loginSceneDidLogin(with userName: String) {
        delegate?.loginModuleDidLogin(with: userName)
    }
}
