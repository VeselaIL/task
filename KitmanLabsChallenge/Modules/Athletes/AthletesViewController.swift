//
//  AthletesViewController.swift
//  KitmanLabsChallenge
//
//  Created by Vesela Ilchevska on 11.07.21.
//

import UIKit

protocol AthletesViewControllerDelegate {
    func loadData(completion: @escaping () -> Void)
    func getAthlete(for: Int) -> Athlete?
    func getAthletesCount() -> Int
    func didSelectCell(at index: Int)
    func getUserName() -> String
    
    var title: String { get }
}

final class AthletesViewController: UIViewController {
    @IBOutlet private weak var tableView: UITableView!

    private var activityView: UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(style: .medium)
        return activityView
    }()

    fileprivate var viewModel: AthletesViewControllerDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = viewModel.title
        configureTableView()
        configureActivityView()
        activityView.startAnimating()
        viewModel.loadData { [weak self] in
            DispatchQueue.main.async {
                self?.activityView.stopAnimating()
                self?.tableView.reloadData()
            }
        }
    }
    
    private func configureActivityView() {
        activityView.center = view.center
        activityView.color = .gray
        view.addSubview(activityView)
    }

    private func configureTableView() {
        tableView.register(ImageListItem.self, forCellReuseIdentifier: "\(ImageListItem.self)")
    }
}

// MARK: -UITableViewDataSource

extension AthletesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getAthletesCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(ImageListItem.self)",
                                                       for: indexPath) as? ImageListItem,
              let athlete = viewModel.getAthlete(for: indexPath.row) else {
            return UITableViewCell()
        }
        cell.selectionStyle = .none
        cell.configure(with: athlete)
        return cell
    }
}

// MARK: -UITableViewDelegate

extension AthletesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelectCell(at: indexPath.row)
    }
}

extension AthletesViewController: StoryboardInstantiatable {
    static var storyboardName: String {
        return Constants.StoryboardNames.athletes
    }
}

struct AthletesSceneConfigurator {
    func createScene(delegate: AthletesSceneDelegate, userName: String) -> AthletesViewController? {
        let controller = AthletesViewController.instantiateFromStoryboard()
        let viewModel = AthletesViewModel(delegate: delegate, userName: userName)
        controller?.viewModel = viewModel
        
        return controller
    }
}
