//
//  AthletesCoordinator.swift
//  KitmanLabsChallenge
//
//  Created by Vesela Ilchevska on 12.07.21.
//

import UIKit

protocol AthletesCoordinatorDelegate: class {
}

final class AthletesCoordinator: Coordinator {
    private weak var delegate: AthletesCoordinatorDelegate?
    private let window: UIWindow?
    private let navigationController: UINavigationController
    private let userName: String

    init(window: UIWindow,
         delegate: AthletesCoordinatorDelegate,
         navigationController: UINavigationController = UINavigationController(),
         userName: String) {
        self.window = window
        self.delegate = delegate
        self.navigationController = navigationController
        self.userName = userName
        super.init()
    }
    
    override func start() {
        window?.rootViewController = navigationController
        showAthletesScene()
    }
    
    private func showAthletesScene() {
        guard let athletesViewController = AthletesSceneConfigurator().createScene(delegate: self,
                                                                                   userName: userName) else {
            return
        }
        navigationController.viewControllers = [athletesViewController]
    }
    
    private func showAthleteDetailsScene(with athlete: Athlete) {
        guard let athleteDetailsViewController = AthleteDetailsSceneConfigurator().createScene(delegate: self,
                                                                                               athlete: athlete) else {
            return
        }
        navigationController.viewControllers.append(athleteDetailsViewController)
    }
}

extension AthletesCoordinator: AthletesSceneDelegate {
    func sceneDidSelectAthlete(_ athlete: Athlete) {
        showAthleteDetailsScene(with: athlete)
    }
}

extension AthletesCoordinator: AthleteDetailsSceneDelegate {}
