//
//  AthletesViewModel.swift
//  KitmanLabsChallenge
//
//  Created by Vesela Ilchevska on 11.07.21.
//

import Foundation

protocol AthletesSceneDelegate: class {
    func sceneDidSelectAthlete(_ athlete: Athlete)
}

final class AthletesViewModel {

    private var athletes: [Athlete] = []
    private weak var delegate: AthletesSceneDelegate?
    private var athletesProducer: AthletesProducer
    private var userName: String
    
    init(delegate: AthletesSceneDelegate,
         userName: String,
         athletesProducer: AthletesProducer = AthletesProducer()) {
        self.delegate = delegate
        self.userName = userName
        self.athletesProducer = athletesProducer
    }
}

extension AthletesViewModel: AthletesViewControllerDelegate {
    
    var title: String {
        return "Athletes"
    }

    func getUserName() -> String {
        return userName
    }

    func didSelectCell(at index: Int) {
        guard let athlete = athletes[safe: index] else {
            return
        }
        delegate?.sceneDidSelectAthlete(athlete)
    }
    
    func getAthlete(for index: Int) -> Athlete? {
        return athletes[safe: index]
    }

    func loadData(completion: @escaping () -> Void) {
        athletesProducer.fetchAthletes { result in
            switch result {
            case .success(let athletes):
                self.athletes = athletes
                completion()
            case .failure( _), .none:
                break
            }
        }
    }
    
    func getAthletesCount() -> Int {
        return athletes.count
    }

}
