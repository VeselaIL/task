//
//  StoryboardInstantiatable.swift
//  KitmanLabsChallenge
//
//  Created by Vesela Ilchevska on 11.07.21.
//

import UIKit

public protocol StoryboardInstantiatable where Self: UIViewController {
    
    /* Returns the name of the storyboard where the view of the ViewController is */
    static var storyboardName: String { get }
    
    static func instantiateFromStoryboard(withName storyboardName: String) -> Self?
}

public extension StoryboardInstantiatable {
    static func instantiateFromStoryboard(withName storyboardName: String = Self.storyboardName) -> Self? {
        return UIStoryboard.init(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: String(describing: self)) as? Self
    }
}
