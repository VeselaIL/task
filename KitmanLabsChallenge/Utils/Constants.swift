//
//  Constants.swift
//  KitmanLabsChallenge
//
//  Created by Vesela Ilchevska on 10.07.21.
//

import Foundation

struct Constants {
    enum StoryboardNames {
        static let splash = "Splash"
        static let login = "Login"
        static let athletes = "Athletes"
        static let athleteDetails = "AthleteDetails"
    }
}
