//
//  UIColor+colors.swift
//  KitmanLabsChallenge
//
//  Created by Vesela Ilchevska on 12.07.21.
//

import UIKit

extension UIColor {
    static let lightBlue = UIColor(red: 28/255, green: 170/255, blue: 231/255, alpha: 1)
}
