//
//  AppFont.swift
//  KitmanLabsChallenge
//
//  Created by Vesela Ilchevska on 12.07.21.
//

import UIKit

struct AppFont {
    
    static func thin(size: CGFloat) -> UIFont {
        return UIFont(name: "SFProRounded-Thin", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func regular(size: CGFloat) -> UIFont {
        return UIFont(name: "SFProRounded-Regular", size: size) ?? UIFont.systemFont(ofSize: size)
    }
}
