//
//  Athletes.swift
//  KitmanLabsChallenge
//
//  Created by Vesela Ilchevska on 10.07.21.
//

import UIKit

struct Athletes: Codable {
    let athletes: [Athlete]
}

struct Athlete: Codable {
    let id: Int
    let firstName: String
    let lastName: String
    let username: String
    let image: Image
    let squadIDS: [Int]
    
    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case lastName = "last_name"
        case username
        case image
        case squadIDS = "squad_ids"
    }
}

struct Image: Codable {
    let url: String
}


struct AthleteHelper {
    let athlete: Athlete
    let squads: [Squad]
}
