//
//  UserData.swift
//  KitmanLabsChallenge
//
//  Created by Vesela Ilchevska on 12.07.21.
//

import Foundation

struct UserData: Codable {
    let username: String
    let password: String?
}
