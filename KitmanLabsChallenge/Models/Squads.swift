//
//  Squads.swift
//  KitmanLabsChallenge
//
//  Created by Vesela Ilchevska on 10.07.21.
//

import Foundation

struct Squads: Codable {
    let squads: [Squad]
}

struct Squad: Codable {
    let id: Int
    let name: String
    let organisationID: Int
    let updatedAt: Date?
    let createdAt: Date?

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case organisationID = "organisation_id"
        case updatedAt = "updated_at"
        case createdAt = "created_at"
    }
}
