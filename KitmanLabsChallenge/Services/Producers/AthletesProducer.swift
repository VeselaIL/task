//
//  NetworkService.swift
//  KitmanLabsChallenge
//
//  Created by Vesela Ilchevska on 10.07.21.
//

import UIKit
import Kingfisher

protocol AthletesProducerProtocol: class {
    func fetchAthletes(completion: @escaping ((Result<[Athlete], Error>?) -> Void))
    func fetchSquads(completion: @escaping((Result<[Squad], Error>?) -> Void))
    func postUserData(userData: UserData, completion: @escaping(Result<String, Error>?) -> Void)
}

final class AthletesProducer: AthletesProducerProtocol {
    private enum HttpMethod: String {
        case post = "POST"
        case get = "GET"
    }

    private enum Constants {
        static let athletePath = "athletes"
        static let squads = "squads"
        static let session = "session"
    }

    fileprivate let urlSession = URLSession.shared
    fileprivate let assetsProvider = KingfisherManager.shared
    
    func fetchAthletes(completion: @escaping (Result<[Athlete],Error>?) -> Void) {
        guard let url = createURL(path: Constants.athletePath) else {
            return
        }
        urlSession.dataTask(with: url) { data, _, error in
            if let data = data,
               let result = try? JSONDecoder().decode(Athletes.self, from: data) {
                completion(.success(result.athletes))
            }
            if let error = error {
                completion(.failure(error))
                return
            }
        }.resume()
    }

    func fetchSquads(completion: @escaping((Result<[Squad], Error>?) -> Void)) {
        guard let url = createURL(path: Constants.squads) else {
            return
        }
        let jsoNDecoder = createJSONDecoderWithDate()
        urlSession.dataTask(with: url) { data, _, error in
            if let data = data,
               let result = try? jsoNDecoder.decode(Squads.self, from: data) {
                completion(.success(result.squads))
            }
            if let error = error {
                completion(.failure(error))
                return
            }
        }.resume()
    }
    
    func postUserData(userData: UserData, completion: @escaping(Result<String, Error>?) -> Void) {
        guard let url = createURL(path: Constants.session) else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = HttpMethod.post.rawValue
        
        let jsonEncoder = JSONEncoder()
        jsonEncoder.outputFormatting = .prettyPrinted
        
        do {
            let jsonData = try jsonEncoder.encode(userData)
            request.httpBody = jsonData
        } catch {
            completion(.failure(NSError.init()))
        }
            
        urlSession.dataTask(with: request) { data, _, error in
            if let data = data,
               let result = try? JSONDecoder().decode(UserData.self, from: data) {
                completion(.success(result.username))
            }
            if let error = error {
                completion(.failure(error))
            }
        }.resume()
    }
    
    private func createJSONDecoderWithDate() -> JSONDecoder {
        let decoder = JSONDecoder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        return decoder
    }

    private func createURL(path: String) -> URL? {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "kml-tech-test.glitch.me"
        components.path = "/\(path)"
        return components.url
    }
}
