//
//  AppCoordinator.swift
//  KitmanLabsChallenge
//
//  Created by Vesela Ilchevska on 11.07.21.
//

import UIKit

class AppCoordinator: Coordinator {

    // MARK: Properties
    private let window: UIWindow
    
    // MARK: Coordinator
    init(window: UIWindow?) {
        self.window = window ?? UIWindow(frame: UIScreen.main.bounds)
        super.init()
    }
    
    override func start() {
        guard let splashScreen = SplashSceneConfigurator().createScene(delegate: self) else { return }
        window.rootViewController = splashScreen
        window.makeKeyAndVisible()
    }
    
    private func showLoginModule() {
        let loginCoordinator = LoginCoordinator(window: window, delegate: self)
        addChildCoordinator(loginCoordinator)
        loginCoordinator.start()
    }

    private func showAthletesModule(with userName: String) {
        let athletesCoordinator = AthletesCoordinator(window: window,
                                                      delegate: self,
                                                      navigationController: UINavigationController(),
                                                      userName: userName)
        addChildCoordinator(athletesCoordinator)
        athletesCoordinator.start()
    }
}

extension AppCoordinator: SplashSceneDelegate {
    func splashSceneDidFinish() {
        showLoginModule()
    }
}

extension AppCoordinator: LoginCoordinatorDelegate {
    func loginModuleDidLogin(with userName: String) {
        showAthletesModule(with: userName)
    }
}

extension AppCoordinator: AthletesCoordinatorDelegate {}
