//
//  Coordinator.swift
//  KitmanLabsChallenge
//
//  Created by Vesela Ilchevska on 11.07.21.
//

import Foundation

protocol CoordinatableViewModel {
    
    /// Starts the viewModel logic
    func start()
}

extension CoordinatableViewModel {
    
    func start() {
        
    }
}

class Coordinator {
    
    private(set) var childCoordinators: [Coordinator] = []
    weak var parentCoordinator: Coordinator?
    
    /// Start The Coordinator !!! SHOULD BE OVERRIDEN IN EACH SUBCLASS !!!
    func start() {
        preconditionFailure("This method needs to be overriden by concrete subclass.")
    }
    
    /// Finish The Coordinator !!! SHOULD BE OVERRIDEN IN EACH SUBCLASS !!!
    func finish() {
        /// Finish The Coordinator !!!
        
        /// The base class automatically handles memory management:
        
        ///- removing `self` from childCoordinator's array of the parent coordinator
        
        /// Subclasses should provide custom navigation action: pop/dimiss/etc...
        
        parentCoordinator?.removeChildCoordinator(self)
    }
    
    /// Adds a child coordinator to the current childCoordinators array
    ///
    /// - Parameter coordinator: the coordinator to add
    func addChildCoordinator(_ coordinator: Coordinator) {
        childCoordinators.append(coordinator)
    }
    
    func addChildCoordinators(_ coordinators: [Coordinator]) {
        childCoordinators.append(contentsOf: coordinators)
    }
    
    /// Removes a child coordinator if such exists from the childCoordinators array
    ///
    /// - Parameter coordinator: the coordinator to remove
    func removeChildCoordinator(_ coordinator: Coordinator) {
        if let index = childCoordinators.firstIndex(of: coordinator) {
            childCoordinators.remove(at: index)
        } else {
            print("Couldn't remove coordinator: \(coordinator). It's not a child coordinator.")
        }
    }
    
    /// Removes all child coordinators of a generic type if they exist from the childCoordinators array
    ///
    /// - Parameter type: the type by which the array is filtered
    func removeAllChildCoordinatorsWith<T>(type: T.Type) {
        childCoordinators = childCoordinators.filter { $0 is T  == false }
    }
    
    /// Removes all child coordinators from the childCoordinators array
    func removeAllChildCoordinators() {
        childCoordinators.removeAll()
    }
    
    /// Returns the first parent coordinator of passed type
    func firstParent<T: Coordinator>(of type: T.Type) -> T? {
        if let parentOfType = parentCoordinator as? T {
            return parentOfType
        }
        
        return parentCoordinator?.firstParent(of: type)
    }
    
    /// Returns the first child coordinator of passed type, checking the self's child coordinators
    func firstChildCoordinator<T: Coordinator>(of type: T.Type) -> T? {
        for coordinator in childCoordinators {
            if let childOfType = coordinator as? T {
                return childOfType
            }
        }
        
        return nil
    }
    
    /// Returns the first child coordinator of passed type, recursively checking all of the child coordinators children
    func firstChildCoordinatorRecursive<T: Coordinator>(of type: T.Type) -> T? {
        for coordinator in childCoordinators {
            if let childOfType = coordinator as? T {
                return childOfType
            }
            
            if !coordinator.childCoordinators.isEmpty {
                return coordinator.firstChildCoordinatorRecursive(of: type)
            }
        }
        
        return nil
    }
}

extension Coordinator: Equatable {
    
    static func == (lhs: Coordinator, rhs: Coordinator) -> Bool {
        return lhs === rhs
    }
    
}
